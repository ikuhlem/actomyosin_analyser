import os
import numpy as np
from typing import Dict, Tuple
from actomyosin_analyser.file_io.afines_converter import AfinesConverter
from actomyosin_analyser.file_io.hdf5_bead_data_reader import HDF5BeadDataReader
from afines_reader.actins_reader import ActinsReader
from afines_reader.motors_reader import MotorsReader
from afines_reader.config_reader import ConfigReader
from afines_reader.model.actins import Actins
from afines_reader.model.motors import Motors

HERE = os.path.dirname(os.path.abspath(__file__))

AFINES_DIR = HERE + "/AFINES_sample_data"
H5_FILE    = HERE + "/tmp_converted.h5"


def read_actins_from_original_files() -> ActinsReader:
    ar = ActinsReader(AFINES_DIR + "/txt_stack/actins.txt")
    ar.read()
    return ar


def test_afines_converter():
    try:
        os.remove(H5_FILE)
        print("Removed converted file of previous test")
    except FileNotFoundError:
        print("No converted file found at beginning of test")
    conv = AfinesConverter(AFINES_DIR)
    conv.convert(H5_FILE)

    h5_reader = HDF5BeadDataReader(H5_FILE)
    actins_orig  = read_actins_from_original_files()
    _assert_time_stamps_are_identical(h5_reader, actins_orig)
    _assert_actins_are_identical(h5_reader, actins_orig)
    _assert_motors_are_identical(h5_reader, actins_orig)

    os.remove(H5_FILE)

def _assert_time_stamps_are_identical(h5_reader: HDF5BeadDataReader, actins_reader: ActinsReader):
    print("called: _assert_time_stamps_are_identical")
    t_steps_h5 = h5_reader.data_set_steps
    t_orig = np.array(sorted(actins_reader.actins.keys()))
    config_reader = ConfigReader(h5_reader.h5_file.attrs["CONFIG"])
    dt = config_reader.get_time_step()
    print(dt)
    print(t_steps_h5[:])
    print(t_orig)
    assert np.allclose(t_steps_h5[:] * dt, t_orig)
    
def _read_motors_from_original_files() -> Tuple[
        Dict[float, Motors],
        Dict[float, Motors]]:
    amotors_reader = MotorsReader(AFINES_DIR + "/txt_stack/amotors.txt")
    pmotors_reader = MotorsReader(AFINES_DIR + "/txt_stack/pmotors.txt")
    amotors_reader.read()
    pmotors_reader.read()
    return amotors_reader.motors, pmotors_reader.motors
    
def _assert_motors_are_identical(h5_reader: HDF5BeadDataReader, actins_reader: ActinsReader):
    print("called: _assert_motors_are_identical")
    amotors, pmotors = _read_motors_from_original_files()
    ds_motor_coords = h5_reader.h5_file["motor_coordinates"]
    ds_motor_links  = h5_reader.h5_file["motor_links"]
    ds_xlink_coords = h5_reader.h5_file["crosslink_coordinates"]
    ds_xlink_links  = h5_reader.h5_file["crosslink_links"]

    for i, ti in enumerate(sorted(amotors.keys())):
        amotors_i = amotors[ti]
        pmotors_i = pmotors[ti]

        assert (amotors_i.x == ds_motor_coords[i, :, 0]).all()
        assert (amotors_i.y == ds_motor_coords[i, :, 1]).all()
        assert (amotors_i.dx == ds_motor_coords[i, :, 2]).all()
        assert (amotors_i.dy == ds_motor_coords[i, :, 3]).all()

        assert (pmotors_i.x == ds_xlink_coords[i, :, 0]).all()
        assert (pmotors_i.y == ds_xlink_coords[i, :, 1]).all()
        assert (pmotors_i.dx == ds_xlink_coords[i, :, 2]).all()
        assert (pmotors_i.dy == ds_xlink_coords[i, :, 3]).all()

        _assert_motor_links_are_identical(ds_motor_links[i], amotors_i, actins_reader.actins[ti])
        _assert_motor_links_are_identical(ds_xlink_links[i], pmotors_i, actins_reader.actins[ti])


def _assert_motor_links_are_identical(motor_links: np.ndarray, motors: Motors, actins: Actins):
    for i, ml in enumerate(motor_links):

        if ml[0] == -1:
            assert motors.fidx0[i] == -1
        else:
            assert actins.fidx[ml[0]] == motors.fidx0[i]
            beads_with_fidx0 = np.where(actins.fidx == actins.fidx[ml[0]])[0]
            assert beads_with_fidx0[motors.lidx0[i]] == ml[0]

        if ml[1] == -1:
            assert motors.fidx1[i] == -1
        else:
            assert actins.fidx[ml[1]] == motors.fidx1[i]
            beads_with_fidx1 = np.where(actins.fidx == actins.fidx[ml[1]])[0]
            assert beads_with_fidx1[motors.lidx1[i]] == ml[1]
    
def _assert_actins_are_identical(h5_reader: HDF5BeadDataReader, actins_orig: ActinsReader):
    print("called: _assert_actins_are_identical")
    actins_h5     = h5_reader.data_set_coords
    filaments_h5  = h5_reader.data_set_filaments
    bead_links_h5 = h5_reader.data_set_links

    for i, ti in enumerate(sorted(actins_orig.actins.keys())):
        actins_orig_i = actins_orig.actins[ti]
        assert (actins_h5[i, :, 0] == actins_orig_i.x).all()
        assert (actins_h5[i, :, 1] == actins_orig_i.y).all()
        
        for f in filaments_h5[i]:
            fidx = f[0]
            beads_with_fidx = np.where(actins_orig_i.fidx == fidx)[0]
            assert f[1] == beads_with_fidx[0]
            assert f[2] == beads_with_fidx[-1]
            assert (np.arange(f[1], f[2] + 1) == beads_with_fidx).all()

            _assert_single_filament_is_complete(bead_links_h5[i], f[1], f[2])

def _assert_single_filament_is_complete(bead_links: np.ndarray, first_bead: int, last_bead: int):
    assert bead_links[first_bead, 1] == first_bead + 1
    assert bead_links[first_bead, 0] == -1
    for j in range(first_bead + 1, last_bead):
        assert bead_links[j, 1]  == j + 1
        assert bead_links[j, 0] == j - 1
    assert bead_links[last_bead, 1] == -1
    assert bead_links[last_bead, 0] == last_bead - 1
