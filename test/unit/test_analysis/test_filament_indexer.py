import numpy as np
from actomyosin_analyser.analysis.filament_indexer import FilamentIndexer


def test_t_index_creation_and_termination():

    ia0 = np.array([])
    ia1 = np.array([12, 140])
    ia2 = np.array([12, 140, 500])
    ia3 = np.array([140, 500])
    ia4 = np.array([])
    
    fi = FilamentIndexer(0)
    
    for t, ia in enumerate([ia0, ia1, ia2, ia3]):
        fi.set_indices_at_t_index(t, ia)

    assert fi.t_index_creation == 1
    assert fi.t_index_termination is None

    fi1 = FilamentIndexer(1)

    for t, ia in enumerate([ia0, ia1, ia2, ia3, ia4]):
        fi1.set_indices_at_t_index(t, ia)

    assert fi1.t_index_creation == 1
    assert fi1.t_index_termination == 4
