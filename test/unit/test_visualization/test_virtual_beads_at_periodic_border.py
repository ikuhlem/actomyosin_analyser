import numpy as np
from actomyosin_analyser.visualization.virtual_beads_at_periodic_border import (_compute_shifts_array,
                                                                                _sort_conflicting_shifts,
                                                                                split_filament_coords_at_borders)

def test_compute_shifts_array():

    bead_coords = np.array([[0.7, 0.6, 0.5],
                            [0.85, 0.7, 0.5],
                            [0.95, 0.9, 0.5],
                            [0.1, 0.08, 0.5],
                            [0.3, 0.9, 0.5]])
    box = [1.0, 1.0, 1.0]

    diff = bead_coords[1:] - bead_coords[:-1]
    # diff = [[ 0.15  0.1   0.  ]  --> shifts_array = [[ 0.0  0.0  0.0]
    #         [ 0.1   0.2   0.  ]                      [ 0.0  0.0  0.0]
    #         [-0.85 -0.82  0.  ]                      [ 1.0  1.0  0.0]
    #         [ 0.2   0.82  0.  ]]                     [ 0.0 -1.0  0.0]]

    shifts_array = _compute_shifts_array(diff, box)

    assert  1.0 == shifts_array[2, 0]
    assert  1.0 == shifts_array[2, 1]
    assert -1.0 == shifts_array[3, 1]
    assert shifts_array[0, :].all() == 0.0
    assert shifts_array[1, :].all() == 0.0
    
    
def test_sort_conflicting_shifts():

    bead_coords = np.array([[0.7, 0.6, 0.5],
                            [0.85, 0.7, 0.5],
                            [0.9, 0.95, 0.5],
                            [0.08, 0.1, 0.5],
                            [0.3, 0.9, 0.5]])
    box = [1.0, 1.0, 1.0]

    diff = bead_coords[1:] - bead_coords[:-1]    

    shifts_array = _compute_shifts_array(diff, box)
    shifts_at = np.where(shifts_array != 0)

    shifts_at_n, shifts_at_i, r_cuts = _sort_conflicting_shifts(shifts_at[0], shifts_at[1],
                                                                bead_coords, diff,
                                                                shifts_array, box)    
    assert len(shifts_at_n) == 3
    assert len(shifts_at_i) == 3
    assert len(r_cuts)      == 3

    assert (2, 1) == (shifts_at_n[0], shifts_at_i[0])
    assert (2, 0) == (shifts_at_n[1], shifts_at_i[1])
    assert (3, 1) == (shifts_at_n[2], shifts_at_i[2])    
    
def test_split_filament_coords_at_borders():

    bead_coords = np.array([[0.7, 0.6, 0.5],
                            [0.85, 0.7, 0.5],
                            [0.95, 0.9, 0.5],
                            [0.1, 0.08, 0.5],
                            [0.3, 0.9, 0.5]])
    box = [1.0, 1.0, 1.0]

    coordinate_sets = split_filament_coords_at_borders(bead_coords, box)

    assert 4 == len(coordinate_sets)
    coord_set = coordinate_sets[0]
    assert 4 == len(coord_set)
    assert (coord_set[0:3] == bead_coords[0:3]).all()
    assert np.allclose(np.array([1.0, 0.96, 0.5]), coord_set[3])
    coord_set = coordinate_sets[1]
    assert 2 == len(coord_set)
    assert np.allclose(np.array([0.0, 0.96, 0.5]), coord_set[0])
    assert np.allclose(np.array([0.0333333333333, 1.0, 0.5]), coord_set[1])
    coord_set = coordinate_sets[2]
    assert 3 == len(coord_set)
    assert np.allclose(np.array([0.0333333333333, 0.0, 0.5]), coord_set[0])
    assert (coord_set[1] == bead_coords[3]).all()
    assert np.allclose(np.array([0.18888888888, 0.0, 0.5]), coord_set[2])
    coord_set = coordinate_sets[3]
    assert 2 == len(coord_set)
    assert np.allclose(np.array([0.18888888888, 1.0, 0.5]), coord_set[0])
    assert (coord_set[1] == bead_coords[4]).all()
